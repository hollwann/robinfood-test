# Best pizza
## Test front end Robinfood

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

En este repositorio se encuentra la solución para la prueba de frontend, a continuación se hablara sobre la estructura del repositorio y como probarlo.


## Estructura

- El repositorio tiene una estructura de Atomic Design para dar una mayor entendimiento del codigo, es decir que los componentes estan divididos en: 
  - atomos: componentes muy basicos
  - moleculas: componentes creados a partir de atomos
  - organismos: componentes creados con atomos y moleculas
  - templates: plantilla para posicionar los componentes de cada vista.
- Todos los componentes son completamente stateless y solo dependen de las propiedades que pasa su padre hacia ellos.
- El desarrollo se realizó en la rama develop y en la main se encuentra unicamente la version final.


## Como probar
### Offline
- Clonar el repositorio en la rama main.
- instalar dependencias con el comando `npm install`
- correr el respositorio con el comando `npm run start`
- abrir en el navegador la url `http://localhost:3000`

### Online
- ir a la url [test-robinfood.web.app](https://test-robinfood.web.app)
