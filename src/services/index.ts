import { Store, User } from '../types'
import axios from 'axios'

const API_URL =
    'https://pruebas-muy-candidatos.s3.us-east-2.amazonaws.com/RH.json'

export const getUsers = (): Promise<{ isError: boolean; data: User[] }> => {
    return axios
        .get(API_URL)
        .then((response) => {
            return {
                data: response.data.response.users,
                isError: false,
            }
        })
        .catch(() => ({ data: [], isError: true }))
}

export const getStores = (): Promise<{ isError: boolean; data: Store[] }> => {
    return axios
        .get(API_URL)
        .then((response) => {
            return {
                data: response.data.response.stores,
                isError: false,
            }
        })
        .catch(() => ({ data: [], isError: true }))
}
