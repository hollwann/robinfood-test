export type User = {
    email: string
    id: number
    name: string
    password: string
}

export type Store = {
    address: string
    description: string
    id: number
    name: string
    products: Product[]
}

export type Product = {
    id: number
    name: string
}
