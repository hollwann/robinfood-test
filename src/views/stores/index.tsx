import { Store } from '../../types'
import { getStores } from '../../services'
import { useHistory } from 'react-router'
import BottomBar from '../../components/organisms/bottom-bar'
import Image from '../../components/atoms/image'
import MainTemplate from '../../components/templates/main-template'
import React, { useEffect, useState } from 'react'
import SideMenu from '../../components/organisms/side-menu'
import StoresList from '../../components/organisms/stores-list'
import logo from '../../assets/images/logo.png'
import pizzaImage from '../../assets/images/pizza.png'

const Stores = () => {
    const history = useHistory()
    const [stores, setStores] = useState([] as Store[])
    const [search, setSearch] = useState('')

    const storesFiltered = search
        ? stores.filter((store) =>
              store.name
                  .toLocaleLowerCase()
                  .includes(search.toLocaleLowerCase())
          )
        : stores

    useEffect(() => {
        getStores().then((response) => {
            if (response.isError) return
            setStores(response.data)
        })
    }, [])

    const goHome = () => history.push('/stores')

    return (
        <MainTemplate
            bottom={<BottomBar />}
            content={
                <StoresList
                    onSearchChange={setSearch}
                    stores={storesFiltered}
                />
            }
            image={
                <Image
                    alt="best pizza"
                    objectFit="contain"
                    src={pizzaImage}
                    width="100%"
                />
            }
            logo={
                <Image
                    objectFit="contain"
                    onClick={goHome}
                    src={logo}
                    width="6vw"
                />
            }
            menu={<SideMenu />}
        />
    )
}

export default Stores
