import { Store } from '../../types'
import { getStores } from '../../services'
import { useHistory, useParams } from 'react-router'
import BottomBar from '../../components/organisms/bottom-bar'
import Image from '../../components/atoms/image'
import MainTemplate from '../../components/templates/main-template'
import React, { useEffect, useState } from 'react'
import SideMenu from '../../components/organisms/side-menu'
import StoreDetails from '../../components/organisms/store-details'
import logo from '../../assets/images/logo.png'
import pizzaImage from '../../assets/images/pizza.png'

const StoreInfo = () => {
    const history = useHistory()
    const { storeId } = useParams() as { storeId?: string }
    const [store, setStore] = useState(undefined as undefined | Store)

    useEffect(() => {
        if (!storeId) return history.replace('/stores')
        getStores().then((response) => {
            if (response.isError) return

            const storeJson = response.data.find((i) => i.id === +storeId)
            if (!storeJson) return history.replace('/stores')

            setStore(storeJson)
        })
    }, [storeId, history])

    const goHome = () => history.push('/stores')

    return (
        <MainTemplate
            bottom={<BottomBar />}
            content={store ? <StoreDetails store={store} /> : <div />}
            image={
                <Image
                    alt="best pizza"
                    objectFit="contain"
                    src={pizzaImage}
                    width="100%"
                />
            }
            logo={
                <Image
                    objectFit="contain"
                    onClick={goHome}
                    src={logo}
                    width="6vw"
                />
            }
            menu={<SideMenu />}
        />
    )
}

export default StoreInfo
