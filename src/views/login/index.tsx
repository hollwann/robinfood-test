import { getUsers } from '../../services'
import { useHistory } from 'react-router'
import Image from '../../components/atoms/image'
import LoginForm from '../../components/organisms/login-form'
import MainTemplate from '../../components/templates/main-template'
import React, { useState } from 'react'
import pizzaImage from '../../assets/images/pizza.png'

const Login = () => {
    const history = useHistory()

    const [user, setUser] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState('')
    const [loading, setLoading] = useState(false)

    const handleLogin = async () => {
        setError('')
        if (!user || !password) return setError('Por favor completa los campos')

        setLoading(true)
        const response = await getUsers()
        setLoading(false)
        if (response.isError) {
            return setError(
                'Verifica tu conexión a internet e intenta de nuevo.'
            )
        }

        const users = response.data
        const userMatch = users.find(
            (item) => item.email === user && item.password === password
        )
        if (!userMatch) return setError('Usuario o contraseña invalidos.')
        sessionStorage.setItem('isLogged', 'true')
        history.replace('/stores')
    }

    const handleRecover = () =>
        setError(
            'Por favor comunicate con un asesor para recuperar tu contraseña'
        )

    return (
        <MainTemplate
            content={
                <LoginForm
                    error={error}
                    handleLogin={handleLogin}
                    handleRecover={handleRecover}
                    isLoading={loading}
                    onPasswordChange={setPassword}
                    onUserChange={setUser}
                />
            }
            image={
                <Image
                    alt="best pizza"
                    objectFit="contain"
                    src={pizzaImage}
                    width="100%"
                />
            }
        />
    )
}

export default Login
