import {
    Redirect,
    Route,
    BrowserRouter as Router,
    Switch,
} from 'react-router-dom'
import Login from './views/login'
import React from 'react'
import StoreInfo from './views/store-info'
import Stores from './views/stores'

const routes = [
    { path: '/login', requiresAuth: false, view: Login },
    { path: '/stores', requiresAuth: true, view: Stores },
    { path: '/store/:storeId', requiresAuth: true, view: StoreInfo },
]

function App() {
    const renderView = (route: {
        requiresAuth: boolean
        path: string
        view: () => JSX.Element
    }) => () => {
        const isLogged = sessionStorage.getItem('isLogged')
        if (isLogged && !route.requiresAuth) return <Redirect to="/stores" />
        else if (!isLogged && route.requiresAuth)
            return <Redirect to="/login" />
        else return <route.view />
    }

    return (
        <Router>
            <Switch>
                {routes.map((route) => (
                    <Route
                        key={route.path}
                        path={route.path}
                        render={renderView(route)}
                    />
                ))}
                <Redirect to="/login" />
            </Switch>
        </Router>
    )
}

export default App
