import './index.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faFacebookSquare,
    faInstagram,
} from '@fortawesome/free-brands-svg-icons'
import { useHistory } from 'react-router'
import Image from '../../atoms/image'
import React from 'react'
import bestPizzaWhite from '../../../assets/images/best-pizza-white.png'

const BottomBar = () => {
    const history = useHistory()
    return (
        <div className="container__bottom-bar">
            <a href="https://facebook.com" target="__blank">
                <FontAwesomeIcon
                    color="white"
                    icon={faFacebookSquare}
                    size="2x"
                />
            </a>
            <a href="https://instagram.com" target="__blank">
                <FontAwesomeIcon
                    color="white"
                    icon={faInstagram}
                    size="2x"
                    style={{ marginLeft: 10 }}
                />
            </a>
            <Image
                alt="best pizza logo"
                className="logo__bottom-bar"
                height="50px"
                objectFit="contain"
                onClick={() => history.push('/stores')}
                src={bestPizzaWhite}
            />
        </div>
    )
}

export default BottomBar
