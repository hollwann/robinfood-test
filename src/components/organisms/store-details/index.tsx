import './index.css'
import { Link } from 'react-router-dom'
import { Store } from '../../../types'
import FlipImage from '../../molecules/flip-image'
import React from 'react'

type Props = {
    store: Store
}

const requestImagePizza = require.context(
    '../../../assets/images/pizzas',
    true,
    /.png$/
)

const StoreDetails = ({ store }: Props) => {
    return (
        <div className="container__store-details">
            <Link className="go-back__store-details" to="/stores">
                {'< Volver'}
            </Link>
            <span className="name__store-details">{store.name}</span>
            <span className="address__store-details">{store.address}</span>
            <span className="description__store-details">
                {store.description}
            </span>
            <span className="products-text__store-details">
                Nuestros productos
            </span>
            <div className="products-container__store-details">
                {store.products.map((product) => (
                    <FlipImage
                        className="product__store-details"
                        image={requestImagePizza(`./${product.id}.png`).default}
                        key={product.id}
                        text={product.name}
                    />
                ))}
            </div>
        </div>
    )
}

export default StoreDetails
