import './index.css'
import { Store } from '../../../types'
import { useHistory } from 'react-router'
import FlipImage from '../../molecules/flip-image'
import React from 'react'

type Props = {
    store: Store
    className: string
}

const requestImageStore = require.context(
    '../../../assets/images/stores',
    true,
    /.png$/
)

const StoreCard = ({ store, className }: Props) => {
    const history = useHistory()
    const onStoreClick = () => {
        history.push(`/store/${store.id}`)
    }
    return (
        <div className={`container__store-card ${className}`}>
            <FlipImage
                image={requestImageStore(`./${store.id}.png`).default}
                onClick={onStoreClick}
                text={store.description}
            />

            <span className="title__store-card">{store.name}</span>
            <span className="address__store-card">{store.address}</span>
        </div>
    )
}

export default StoreCard
