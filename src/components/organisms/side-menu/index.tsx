import './index.css'
import { useHistory } from 'react-router'
import Image from '../../atoms/image'
import React from 'react'
import iconPassword from '../../../assets/icons/ic_password.png'

const SideMenu = () => {
    const history = useHistory()
    const logout = () => {
        sessionStorage.removeItem('isLogged')
        history.replace('/login')
    }

    return (
        <div className="container__side-menu" onClick={logout}>
            <Image className="icon__side-menu" src={iconPassword} />
            <span>Salir</span>
        </div>
    )
}

export default SideMenu
