import './index.css'
import { Store } from '../../../types'
import Input from '../../molecules/input'
import React from 'react'
import StoreCard from '../store-card'
import loupe from '../../../assets/icons/loupe.png'

type Props = {
    stores: Store[]
    onSearchChange: (val: string) => void
}

const StoresList = ({ stores, onSearchChange }: Props) => {
    return (
        <div className="container__stores-list">
            <span className="title__stores-list">&nbsp;Pizzerías&nbsp;</span>
            <span className="stores-text__stores-list">Tiendas</span>
            <span className="choose-text__stores-list">
                Escoge tu pizzería favorita
            </span>
            <Input
                className="search__stores-list"
                icon={loupe}
                onChange={onSearchChange}
                placeholder="Buscar"
            />
            <div className="stores-container__stores-list">
                {stores.length ? (
                    stores.map((store) => (
                        <StoreCard
                            className="store-card__stores-list"
                            key={store.id}
                            store={store}
                        />
                    ))
                ) : (
                    <span className="search__stores-list">
                        No hay resultados para tu busqueda :(
                    </span>
                )}
            </div>
        </div>
    )
}

export default StoresList
