import './index.css'
import Button from '../../molecules/button'
import Image from '../../atoms/image'
import Input from '../../molecules/input'
import React from 'react'
import iconPassword from '../../../assets/icons/ic_password.png'
import iconUser from '../../../assets/icons/ic_user.png'
import loginBestPizzaImage from '../../../assets/images/login-best-pizza.png'

type Props = {
    onUserChange: (val: string) => void
    onPasswordChange: (val: string) => void
    handleLogin: () => void
    handleRecover: () => void
    error: string
    isLoading: boolean
}

const LoginForm = ({
    isLoading,
    onUserChange,
    onPasswordChange,
    handleLogin,
    error,
    handleRecover,
}: Props) => {
    return (
        <div className="form-container__login-form">
            <Image
                alt="best pizza logo"
                height="21vh"
                objectFit="contain"
                src={loginBestPizzaImage}
            />
            <span className="title__login-form">Bienvenido</span>
            <span className="subtitle__login-form">
                A las mejores pizzas del país
            </span>
            <Input
                className="input-user__login-form"
                icon={iconUser}
                onChange={onUserChange}
                placeholder="Usuario"
            />
            <Input
                className="input-password__login-form"
                icon={iconPassword}
                onChange={onPasswordChange}
                placeholder="Contraseña"
                type="password"
            />
            <span className="error__login-form">{error}</span>
            <span className="recovery__login-form" onClick={handleRecover}>
                ¿Olvidaste tu contraseña?
            </span>
            <Button
                className="button__login-form"
                color="#f9c444"
                isLoading={isLoading}
                onClick={handleLogin}
            />
        </div>
    )
}

export default LoginForm
