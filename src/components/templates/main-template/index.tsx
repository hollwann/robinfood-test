import './index.css'
import React from 'react'

type Props = {
    image: JSX.Element
    content: JSX.Element
    menu?: JSX.Element
    bottom?: JSX.Element
    logo?: JSX.Element
}

const MainTemplate = ({ image, content, menu, bottom, logo }: Props) => {
    return (
        <div className="container__main-template">
            <div className="image-container__main-template">{image}</div>
            <div className="main-container__main-template">
                <div className="content-container__main-template">
                    <div className="info-container__main-template">
                        {content}
                    </div>
                    <div className="menu-container__main-template">{menu}</div>
                </div>
                <div className="bottom-container__main-template">{bottom}</div>
            </div>
            <div className="home-logo__main-template">{logo}</div>
        </div>
    )
}

export default MainTemplate
