import './index.css'
import React from 'react'

type Props = {
    color: React.CSSProperties['backgroundColor']
    isLoading?: boolean
    onClick: () => void
    className: string
}

const Button = ({ isLoading, color, onClick, className }: Props) => {
    return (
        <button
            className={`button__button ${className}`}
            onClick={onClick}
            style={{ backgroundColor: color }}
        >
            {isLoading ? (
                <div className="lds-dual-ring__button" />
            ) : (
                <span className="text-button__button">Iniciar sesión</span>
            )}
        </button>
    )
}

export default Button
