import './index.css'
import Image from '../../atoms/image'
import React from 'react'

type Props = {
    image: string
    text: string
    onClick?: () => void
    className?: string
}

const FlipImage = ({ image, text, onClick, className }: Props) => {
    return (
        <div className={`flip-card ${className}`} onClick={onClick}>
            <div className="flip-card-inner">
                <div className="flip-card-front">
                    <Image
                        height="200px"
                        objectFit="contain"
                        src={image}
                        width="200px"
                    />
                </div>
                <div className="flip-card-back">
                    <span>{text}</span>
                </div>
            </div>
        </div>
    )
}

export default FlipImage
