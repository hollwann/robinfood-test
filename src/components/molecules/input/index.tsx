import './index.css'
import Image from '../../atoms/image'
import React from 'react'

type Props = {
    placeholder: string
    icon: string
    type?: string
    className?: string
    onChange?: (val: string) => void
}

const Input = ({ placeholder, icon, type, className, onChange }: Props) => {
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) =>
        onChange && onChange(e.target.value)

    return (
        <div className={`container__input ${className}`}>
            <input
                className="input__input"
                onChange={handleChange}
                placeholder={placeholder}
                type={type}
            />
            <Image
                alt="logo"
                className="icon__input"
                objectFit="contain"
                src={icon}
                width="1.44rem"
            />
        </div>
    )
}

export default Input
