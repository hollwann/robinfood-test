import React from 'react'

type Props = {
    width?: React.CSSProperties['width']
    objectFit?: React.CSSProperties['objectFit']
    alt?: string
    src: string
    className?: string
    height?: React.CSSProperties['height']
    onClick?: () => void
}

const Image = ({
    alt,
    src,
    width,
    objectFit,
    className,
    height,
    onClick,
}: Props) => {
    return (
        <img
            alt={alt}
            className={`image ${className}`}
            onClick={onClick}
            src={src}
            style={{ height, objectFit, width }}
        />
    )
}

export default Image
